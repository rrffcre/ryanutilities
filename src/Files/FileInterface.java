package Files;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.imageio.ImageIO;

/**
 * <h1>FileInterface</h1>
 * 
 * @author Ryan Plummer
 */
public class FileInterface {

	protected FileInterface() {
	}

	/**
	 * <h1>writeToTextFile()</h1> This method will write the passed array of text
	 * and print it in a file located at the passed path. If the file already
	 * exists, it will be overwritten. Each string in the text array will be
	 * treated as a new line when writing the file.
	 * 
	 * @param path
	 *          The location of the file to be created. Should be of the form:
	 *          "./../filename" starting at the current directory.
	 * @param text
	 *          The text array should consist of string representing the lines of
	 *          the text file.
	 * @return Whether the execution was completed successfully.
	 */
	public static boolean writeToTextFile(String path, Collection <String> text) {
		PrintWriter printWriter = null;
		boolean success;
		try {
			printWriter = new PrintWriter(new File(path));
			for (String line : text.toArray(new String[0])) {
				printWriter.println(line);
			}
			success = true;
		} catch (FileNotFoundException | NullPointerException e) {
			System.err.println(e);
			success = false;
		} finally {
			if (printWriter != null) {
				printWriter.close();
			}
		}
		return success;
	}

	/**
	 * <h1>readFromTextFile</h1> This method will read the text from the file at
	 * the passed path.
	 * 
	 * @param path
	 *          The location of the file to be created. Should be of the form:
	 *          "./../filename" starting at the current directory.
	 * @return A list containing the text from the file.
	 */
	public static List <String> readFromTextFile(String path) {
		List <String> text = null;
		try {
			text = Files.readAllLines(Paths.get(path), StandardCharsets.UTF_8);
		} catch (IOException ioe) {
			System.err.println(ioe);
			text = new ArrayList <String>();
		}
		return text;
	}

	/**
	 * <h1>writeToObjectFile</h1> This method will write the passed object to the
	 * file at the passed path. If the file already exists and append is set to
	 * false, the file will be replaced with the new object.
	 * 
	 * @param path
	 *          The location of the file to be created. Should be of the form:
	 *          "./../filename" starting at the current directory.
	 * @param object
	 *          The object to be stored in the file.
	 * @param append
	 *          Whether the new object will be appended.
	 * @return Whether the execution was successful.
	 */
	public static boolean writeToObjectFile(String path, Object object, boolean append) {
		ObjectOutput objectOutput = null;
		boolean success;
		try {
			objectOutput = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(path, append)));
			objectOutput.writeObject(object);
			success = true;
		} catch (NullPointerException | IOException e) {
			System.err.println(e);
			success = false;
		} finally {
			if (objectOutput != null) {
				try {
					objectOutput.close();
				} catch (IOException ioe) {
					System.err.println(ioe);
				}
			}
		}
		return success;
	}

	/**
	 * <h1>readFromObjectFile</h1> This method will read an object from the file
	 * at the passed path.
	 * 
	 * @param path
	 *          The location of the file to be created. Should be of the form:
	 *          "./../filename" starting at the current directory.
	 * @return The object read from the file.
	 */
	public static Object readFromObjectFile(String path) {
		ObjectInput objectInput = null;
		Object object = null;
		if (new File(path).exists()) {
			try {
				objectInput = new ObjectInputStream(new BufferedInputStream(new FileInputStream(path)));
				object = objectInput.readObject();
			} catch (ClassNotFoundException | IOException e) {
				System.err.println(e);
			} finally {
				if (objectInput != null) {
					try {
						objectInput.close();
					} catch (IOException ioe) {
						System.err.println(ioe);
					}
				}
			}
		}
		return object;
	}

	/**
	 * <h1>writeToBinaryFile</h1> This method will write the passed binary data to
	 * the file at the passed path. If the file already exists and append is set
	 * to false, the file will be replaced with the new object.
	 * 
	 * @param path
	 *          The location of the file to be created. Should be of the form:
	 *          "./../filename" starting at the current directory.
	 * @param data
	 *          The binary data to be stored in the file.
	 * @param append
	 *          Whether the new data will be appended.
	 * @return Whether the execution was successful.
	 */
	public static boolean writeToBinaryFile(String path, byte[] data, boolean append) {
		FileOutputStream fileOutput = null;
		boolean success;
		try {
			fileOutput = new FileOutputStream(path, append);
			fileOutput.write(data);
			success = true;
		} catch (IOException ioe) {
			System.err.println(ioe);
			success = false;
		} finally {
			if (fileOutput != null) {
				try {
					fileOutput.close();
				} catch (IOException ioe) {
					System.err.println(ioe);
				}
			}
		}
		return success;
	}

	/**
	 * <h1>readFromBinaryFile</h1> This method will read an object from the file
	 * at the passed path. Maximum readable filesize is 2.14 Gb
	 * 
	 * @param path
	 *          The location of the file to be read. Should be of the form:
	 *          "./../filename" starting at the current directory.
	 * @return The binary data read from the file.
	 */
	public static byte[] readFromBinaryFile(String path) {
		FileInputStream fileInput = null;
		byte[] data = null;
		if (new File(path).exists()) {
			try {
				fileInput = new FileInputStream(path);
				int size = (int) new File(path).length();
				data = new byte[size];
				fileInput.read(data);
			} catch (IOException ioe) {
				System.err.println(ioe);
			} finally {
				if (fileInput != null) {
					try {
						fileInput.close();
					} catch (IOException ioe) {
						System.err.println(ioe);
					}
				}
			}
		}
		return data;
	}

	/**
	 * <h1>writeToImageFile</h1> This method will write the passed image to the
	 * file at the passed path.
	 * 
	 * @param path
	 *          The location of the file to be created. Should be of the form:
	 *          "./../filename" starting at the current directory.
	 * @param image
	 *          The image to be stored in the file.
	 * @return Whether the execution was successful.
	 */
	public static boolean writeToImageFile(String path, BufferedImage image) {
		boolean result = false;
		try {
			result = ImageIO.write(image, path.split(".+\\.")[1], new File(path));
		} catch (IOException ioe) {
			System.err.println(ioe);
		}
		return result;
	}

	/**
	 * <h1>readFromImageFile</h1> This method will read an image from the file at
	 * the passed path.
	 * 
	 * @param path
	 *          The location of the file to be read. Should be of the form:
	 *          "./../filename" starting at the current directory.
	 * @return The image read from the file.
	 */
	public static BufferedImage readFromImageFile(String path) {
		BufferedImage image = null;
		if (new File(path).exists()) {
			try {
				image = ImageIO.read(new File(path));
			} catch (IOException ioe) {
				System.err.println(ioe);
			}
		}
		return image;
	}
}
