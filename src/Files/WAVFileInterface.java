package Files;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.security.InvalidParameterException;

/**
 * <h1>WAVFileInterface</h1> This class is designed for the creation of standard
 * WAV files. See provided link for explanation of methodology.
 * 
 * @author Ryan Plummer
 * @link http://soundfile.sapp.org/doc/WaveFormat/
 */
public class WAVFileInterface {
	private String filePath;
	private FileOutputStream fileOut;

	private int numberOfChannels;
	private int sampleRate;
	private int bitDepth;
	private int numberOfSamples;

	/**
	 * <h1>WAVFileInterface</h1> This constructor allows the creation of a WAV
	 * file at the specified path. If a file already exists at the given path,
	 * the old file is overwrittn.The parameters govern the attributes of the
	 * WAV file for use in creating the file header.
	 * 
	 * @param path
	 *            The location of the file to be created. Should be of the form:
	 *            "./../filename" starting at the current directory.
	 * @param channels
	 *            The number of channels used in the recording process (1 =
	 *            mono, 2 = stereo, etc.)
	 * @param rate
	 *            The sample rate at which the audio was recorded. Some standard
	 *            values are: 44100, 48000, and 96000.
	 * @param depth
	 *            The precision at which the audio data is represented. Some
	 *            standard values are: 8, 16, 24, and 32.
	 */
	public WAVFileInterface(String path, int channels, int rate, int depth) {
		if (depth % 8 != 0)
			throw new InvalidParameterException("Specified bitdepth is invalid.");
		this.filePath = path;
		try {
			this.fileOut = new FileOutputStream(path, false);
			this.fileOut.write(intToBytes(0, 44)); // Space for header
		} catch (IOException e) {
			System.err.println(e);
		}

		this.numberOfChannels = channels;
		this.sampleRate = rate;
		this.bitDepth = depth;
	}

	/**
	 * <h1>addSample</h1> This method appends a new sample onto the end of the
	 * new file. The sample should contain a positive number for each channel.
	 * The precision of each number is limited to the bit depth of the WAV file.
	 * 
	 * @param sample
	 *            The values comprising the new sample to be added. The number
	 *            of values must match the number of channels in the WAV file.
	 * @throws InvalidParameterException
	 *             This is thrown if the number of values does not match the
	 *             number of channels.
	 * @return If the operation was successful.
	 */
	public boolean addSample(int... sample) throws InvalidParameterException {
		boolean success;
		if (sample.length != this.numberOfChannels)
			throw new InvalidParameterException("Inconsistant number of channels.");
		try {
			for (int i = 0; i < sample.length; i++) {
				this.fileOut.write(intToBytes(sample[i], this.bitDepth / 8));
			}
			this.numberOfSamples++;
			success = true;
		} catch (IOException e) {
			System.err.println(e);
			success = false;
		}
		return success;
	}

	/**
	 * <h1>endFile</h1> This method replaces the empty space left for the header
	 * with the actual values. This also closes the FileOutputStream to prevent
	 * resource leaks. This method must be called once all the samples have been
	 * added.
	 * 
	 * @return If the operation was successful.
	 */
	public boolean endFile() {
		RandomAccessFile file;
		boolean success;
		try {
			this.fileOut.close();
			file = new RandomAccessFile(this.filePath, "rw");
			file.seek(0L);
			file.write("RIFF".getBytes()); // ChunkID
			file.write(intToBytes(36 + (this.numberOfSamples * this.numberOfChannels * this.bitDepth / 8), 4));
			file.write("WAVE".getBytes()); // Format
			file.write("fmt ".getBytes()); // SubchunkID
			file.write(intToBytes(16, 4)); // SubchunkSize (16)
			file.write(intToBytes(1, 2)); // AudioFormat
			file.write(intToBytes(this.numberOfChannels, 2)); // NumChannels
			file.write(intToBytes(this.sampleRate, 4)); // SampleRate
			file.write(intToBytes(this.sampleRate * this.numberOfChannels * this.bitDepth / 8, 4)); // ByteRate
			file.write(intToBytes(this.numberOfChannels * this.bitDepth / 8, 2)); // BlockAlign
			file.write(intToBytes(this.bitDepth, 2)); // BitsPerSample
			file.write("data".getBytes());
			file.write(intToBytes(this.numberOfSamples * this.numberOfChannels * this.bitDepth / 8, 4));
			file.close();
			success = true;
		} catch (IOException e) {
			System.err.println(e);
			success = false;
		}
		return success;
	};

	private byte[] intToBytes(int value, int size) {
		byte[] bytes = new byte[size];
		for (int i = 0; i < size; i++) {
			bytes[i] = (byte) (value >> 8 * i);
		}
		return bytes;
	}
}