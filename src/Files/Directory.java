package Files;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * @Name : Ryan Plummer
 * @Username : rplummer
 * @Session : CSCI 3320 - 001/002
 */
public class Directory {

	private static TreeNode root = new TreeNode("/", null, null);

	private static Scanner input = new Scanner(System.in);

	private static String dir = "dir.txt";

	private static PrintWriter writer;

	/**
	 * writeFile
	 * 
	 * This method recursively writes each entry to the dir file.
	 * 
	 * @param n
	 */
	private static void writeFile(TreeNode n) {
		if (n == null)
			return;
		writer.println(n.getElement());
		writeFile(n.getFirstChild());
		writeFile(n.getNextSibling());
	}

	/**
	 * readFile
	 * 
	 * If the dir file does not exists, nothing will happen. If the file does
	 * exists the lines of the file will be read and used to update the tree.
	 */
	private static void readFile() {
		if (!Files.exists(Paths.get(dir)))
			return;
		List <String> lines;
		try {
			lines = Files.readAllLines(Paths.get(dir), StandardCharsets.UTF_8);
		} catch (IOException e) {
			return;
		}
		for (String line : lines.toArray(new String[0])) {
			addFileIntoDirectory(line);
		}
	}

	/**
	 * printSize
	 * 
	 * This method simply prints the current number of nodes in the tree.
	 */
	private static void printSize() {
		System.out.printf("\nThe size of the directory is %d.\n", root.size());
	}

	/**
	 * printDirectory
	 * 
	 * This will print each node in the tree by calling the listAll method.
	 */
	private static void printDirectory() {
		System.out.println("\nThe directory is displayed as follows:");
		root.listAll(0);
	}

	/**
	 * getUserInput
	 * 
	 * This method will get a value from the user and verify that it is valid.
	 * 
	 * @return
	 */
	private static int getUserInput() {
		String selection;
		while (true) {
			System.out.print("Please give a selection [1-4]: ");
			selection = input.next();
			if (selection.matches("^[0123]$"))
				break;
			else {
				System.out.print("Input is invalid please try again\n");
			}
		}
		Integer selectionInt = Integer.valueOf(selection);
		return selectionInt.intValue();
	}

	/**
	 * addFileFromUser
	 * 
	 * This will continue to takes values from the user and place them into the
	 * tree until "done" is typed.
	 */
	private static void addFileFromUser() {
		System.out.println("To terminate input, type \"done\".");

		input.reset();
		String value = "";
		while (true) {
			value = input.nextLine();
			if (value.equalsIgnoreCase("done"))
				break;
			addFileIntoDirectory(value);
		}
	}

	/**
	 * addFileIntoDirectory
	 * 
	 * This method creates a node in the tree based upon the string passed in
	 * form the user. It is also used when loading from the dir file.
	 */
	private static void addFileIntoDirectory(String line) {

		if (line.trim().equals(""))
			return;

		StringTokenizer tokens = new StringTokenizer(line);

		int n = tokens.countTokens() - 1;

		TreeNode p = root;
		while ((n > 0) && p.isDirectory()) {

			int a = Integer.valueOf(tokens.nextToken());
			p = p.getFirstChild();

			while ((a > 1) && (p != null)) {
				p = p.getNextSibling();
				a--;
			}
			n--;
		}

		String name = tokens.nextToken();

		TreeNode newNode = new TreeNode(name, null, null);
		if (p.getFirstChild() == null) {
			p.setFirstChild(newNode);
		} else {
			p = p.getFirstChild();
			while (p.getNextSibling() != null) {
				p = p.getNextSibling();
			}
			p.setNextSibling(newNode);
		}

	}

	private static class TreeNode {

		private String element;
		private TreeNode firstChild;
		private TreeNode nextSibling;

		public TreeNode(String e, TreeNode f, TreeNode s) {
			this.setElement(e);
			this.setFirstChild(f);
			this.setNextSibling(s);
		}

		public void listAll(int i) {

			for (int k = 0; k < i; k++) {
				System.out.print('\t');
			}

			System.out.println(this.getElement());

			if (this.isDirectory()) {
				TreeNode t = this.getFirstChild();

				while (t != null) {

					t.listAll(i + 1);
					t = t.getNextSibling();
				}

			}
		}

		public int size() {

			int s = 1;

			if (this.isDirectory()) {
				TreeNode t = this.getFirstChild();

				while (t != null) {
					s += t.size();
					t = t.getNextSibling();
				}
			}

			return s;
		}

		public void setElement(String e) {
			this.element = e;
		}

		public String getElement() {
			return this.element;
		}

		public boolean isDirectory() {
			return this.getFirstChild() != null;
		}

		public void setFirstChild(TreeNode f) {
			this.firstChild = f;
		}

		public TreeNode getFirstChild() {
			return this.firstChild;
		}

		public void setNextSibling(TreeNode s) {
			this.nextSibling = s;
		}

		public TreeNode getNextSibling() {
			return this.nextSibling;
		}

	}

}
