package Files;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.Collection;

public class CSVFileInterface {

	public static <T> boolean writeToCSVFile(String path, Collection <T> items, T sample, boolean append) {
		PrintWriter printWriter = null;
		boolean success;
		Field[] fields = sample.getClass().getFields();

		try {
			printWriter = new PrintWriter(new File(path));

			if (!append) {
				String[] names = new String[fields.length];
				for (int i = 0; i < fields.length; i++) {
					names[i] = fields[i].getName();
				}
				printWriter.println(String.join(",", names));
			}

			for (Object item : items.toArray()) {
				String[] values = new String[fields.length];
				for (int i = 0; i < fields.length; i++) {
					Object value;
					try {
						value = fields[i].get(item);
					} catch (IllegalAccessException iae) {
						value = "";
					}
					values[i] = String.valueOf(value);
				}
				printWriter.println(String.join(",", values));
			}
			success = true;
		} catch (FileNotFoundException | NullPointerException e) {
			System.err.println(e);
			success = false;
		} finally {
			if (printWriter != null) {
				printWriter.close();
			}
		}
		return success;
	}
}
