package Graphics;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JMenuBar;

/**
 * MenuBar A simple, mono-depth menu containing the specified buttons.
 * @author Ryan Plummer
 */
public class MenuBar extends JMenuBar {
	private static final long	serialVersionUID	= 6560740106458411923L;

	private List<JButton>		buttons				= new ArrayList<JButton>();

	/**
	 * The default constructor creating a Menubar with the specified buttons.
	 * @param names A arrays of strings used to name the buttons.
	 */
	public MenuBar(String... names) {
		for (String name : names) {
			JButton button = new JButton(name);
			this.buttons.add(button);
			this.add(button);
		}
	}

	/**
	 * addListener Adds the passed listener to a button matching the given name.
	 * @param buttonName The name of the button to which this listener will be
	 * added.
	 * @param listener The listener to be added.
	 * @return If the button exists in this MenuBar.
	 */
	public boolean addListener(String buttonName, ActionListener listener) {
		boolean buttonExists = false;
		for (JButton button : this.buttons) {
			if (button.getText().equals(buttonName)) {
				button.addActionListener(listener);
				buttonExists = true;
				break;
			}
		}
		return buttonExists;
	}
}