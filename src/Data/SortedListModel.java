package Data;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.swing.AbstractListModel;

/**
 * <h1>SortedListModel</h1>
 * 
 * @author java2s.com (edited by Ryan Plummer)
 * @link http://www.java2s.com/Tutorials/Java/Swing/JList/
 *       Create_a_sorted_List_Model_for_JList_in_Java.html
 * 
 */
public class SortedListModel<T extends Comparable <T>> extends AbstractListModel <T> {

	private static final long serialVersionUID = 1L;

	SortedSet <T> model;

	public SortedListModel() {
		model = new TreeSet <T> ();
	}

	public int getSize() {
		return model.size ();
	}

	@SuppressWarnings("unchecked")
	public T getElementAt(int index) {
		return (T) model.toArray ()[index];
	}

	public void addElement(T element) {
		if (model.add (element)) {
			fireContentsChanged (this, 0, getSize ());
		}
	}

	public void addAll(T[] elements) {
		List <T> c = Arrays.asList (elements);
		model.addAll (c);
		fireContentsChanged (this, 0, getSize ());
	}

	public void clear() {
		model.clear ();
		fireContentsChanged (this, 0, getSize ());
	}

	public boolean contains(T element) {
		return model.contains (element);
	}

	public T firstElement() {
		return model.first ();
	}

	public Iterator <T> iterator() {
		return model.iterator ();
	}

	public T lastElement() {
		return model.last ();
	}

	public void update() {
		fireContentsChanged (this, 0, getSize ());
	}

	public boolean removeElement(T element) {
		boolean removed = model.remove (element);
		if (removed) {
			fireContentsChanged (this, 0, getSize ());
		}
		return removed;
	}
}
