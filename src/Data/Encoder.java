package Data;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.InvalidParameterException;
import java.util.BitSet;

/**
 * <h1>Encoder</h1>
 * 
 * @author Ryan Plummer
 *
 */
public class Encoder {
	private static final Charset UTF_8 = StandardCharsets.UTF_8;

	/**
	 * <h1>encode</h1> This method takes a string, and encodes it with a given
	 * key. The string must meet the following criteria:
	 * <li>It must under 32 characters long.</li>
	 * <li>It must consist of standard UTF_8 characters.</li>
	 * <li>It must not contain any spaces.</li>
	 * 
	 * @param string
	 *            The value the will be encoded. Must be shorter than 32 UTF_8
	 *            characters. No spaces (" ") may be contained in the string.
	 * @param key
	 *            A value used the encode the value. Must also be used for
	 *            decoding.
	 * @return A 64 character string of UTF_8 characters.
	 * @throws InvalidParameterException
	 *             If the space is found in the passed string.
	 */
	public String encode(String string, String key) throws InvalidParameterException {
		if (string.contains(" ") || string.length() > 32)
			throw new InvalidParameterException("String does not meet the requirements.");
		string += " " + string.length() + " " + string.hashCode() + " ";
		BitSet valueBits = BitSet.valueOf(string.getBytes());
		BitSet keyBits = BitSet.valueOf(key.getBytes());
		BitSet resultBits = new BitSet(512);
		for (int i = 0; i < resultBits.size(); i++) {
			if (valueBits.get(i % (string.length() * 8)) != keyBits.get(i % (key.length() * 8))) {
				resultBits.set(i);
			}
		}
		byte[] resultBytes = resultBits.toByteArray();
		String result = new String(resultBytes, UTF_8);
		return result;
	}

	/**
	 * <h1>decode</h1> This method uses the given key to decode the encoded
	 * string to return it to its original state.
	 * 
	 * @param encodedString
	 *            A 32 character string encoded by the encode method.
	 * @param key
	 *            A string used to decode the encoded string. Must be the same
	 *            string as was used in the encoding process.
	 * @return The data decoded from the encoded string.
	 */
	public String decode(String encodedString, String key) {
		BitSet valueBits = BitSet.valueOf(encodedString.getBytes());
		BitSet keyBits = BitSet.valueOf(key.getBytes());
		for (int i = 0; i < valueBits.size(); i++) {
			if (keyBits.get(i % (key.length() * 8))) {
				valueBits.flip(i);
			}
		}
		byte[] resultBytes = valueBits.toByteArray();
		String[] result = new String(resultBytes, UTF_8).split(" ");
		if (result[0].length() == Integer.valueOf(result[1]).intValue()
				&& result[0].hashCode() == Integer.valueOf(result[2].trim()).intValue()) {
			return result[0];
		}
		return "Error in decoding: invalid key.";
	}
}