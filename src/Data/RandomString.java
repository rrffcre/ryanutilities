package Data;

import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;

/**
 * @author Ryan Plummer
 */
public class RandomString {
	private SecureRandom random = new SecureRandom();

	/**
	 * @param regex
	 * @param length
	 * @return
	 */
	public String getString(String regex, int length) {
		String string = "";
		while (string.length() < length) {
			byte[] randomByte = new byte[1];
			random.nextBytes(randomByte);
			String randomString = new String(randomByte, StandardCharsets.UTF_8);
			if (randomString.matches(regex)) {
				string += randomString;
			}
		}
		return string;
	}

	/**
	 * @param seed
	 */
	public void setSeed(String seed) {
		random.setSeed(seed.getBytes(StandardCharsets.UTF_8));
		return;
	}
}