package Data;

import java.util.Arrays;
import java.util.Collection;

/**
 * <h1>DataSet</h1>
 * 
 * @author Ryan
 *
 */
public class DataSet {
	private Integer[] set;
	private Integer mean;
	private Integer median;
	private Integer range;
	private Integer stdDev;

	public DataSet(Integer[] set) throws IllegalArgumentException {
		if (set == null || set.length < 1) {
			throw new IllegalArgumentException ();
		}
		this.set = set;
		Arrays.sort (this.set);
	}

	public DataSet(Collection <Integer> set) {
		if (set == null || set.size () < 1) {
			throw new IllegalArgumentException ();
		}
		this.set = (Integer[]) set.toArray ();
		Arrays.sort (this.set);
	}

	public Integer getMean() {
		if (this.mean == null) {
			int sum = 0;
			for (Integer entry : this.set) {
				sum += entry;
			}
			this.mean = sum / this.set.length;
		}
		return this.mean;
	}

	public Integer getMedian() {
		if (this.median == null) {
			int middle = this.set.length / 2;
			if (this.set.length % 2 == 0) {
				this.median = (this.set[middle] + this.set[middle - 1]) / 2;
			} else {
				this.median = this.set[middle];
			}
		}
		return this.median;
	}

	public Integer getRange() {
		if (this.range == null) {
			this.range = this.set[this.set.length] - this.set[0];
		}
		return this.range;
	}

	public Integer getStandardDeviation() {
		if (this.stdDev == null) {
			double totalError = 0;
			for (Integer entry : this.set) {
				totalError += Math.pow ((entry - this.getMean ()), 2);
			}
			this.stdDev = (int) Math.sqrt (totalError / this.set.length);
		}
		return this.stdDev;
	}
}