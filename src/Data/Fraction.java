package Data;

public class Fraction {
	private int numerator;
	private int denominator;

	Fraction(int n) {
		this.numerator = n;
		this.denominator = 1;
	}

	Fraction(int n, int d) {
		this.numerator = n;
		this.denominator = d;
	}

	public Fraction add(Fraction a) {
		int newN = (this.numerator * a.getDenominator()) + (a.getNumerator() * this.denominator);
		int newD = this.denominator * a.getDenominator();
		return new Fraction(newN, newD);
	}

	public Fraction multiply(Fraction a) {
		int newN = this.numerator * a.getNumerator();
		int newD = this.denominator * a.getDenominator();
		return new Fraction(newN, newD);
	}

	public Fraction multiply(int a) {
		return new Fraction(this.numerator * a, this.denominator * a);
	}

	public double getDecimal() {
		return ((double) this.numerator) / ((double) this.denominator);
	}

	public boolean simplify() {
		int gcd = this.getGCD(this.numerator, this.denominator);
		if (gcd == 1) {
			return false;
		} else {
			this.numerator /= gcd;
			this.denominator /= gcd;
			return true;
		}
	}

	public int getNumerator() {
		return this.numerator;
	}

	public int getDenominator() {
		return this.denominator;
	}

	public void setNumerator(int numerator) {
		this.numerator = numerator;
	}

	public void setDenominator(int denominator) {
		this.denominator = denominator;
	}

	@Override
	public String toString() {
		if (this.denominator == 1) {
			return String.valueOf(this.numerator);
		} else {
			return this.numerator + "/" + this.denominator;
		}
	}

	private int getGCD(int a, int b) {
		if (b == 0)
			return a;
		else
			return this.getGCD(b, a % b);
	}
}