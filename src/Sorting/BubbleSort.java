package Sorting;

public class BubbleSort {
	public static <T extends Comparable<T>> T[] sort(T[] array) {
		final long startTime = System.currentTimeMillis();
		for(int i = array.length; i > 0; i--) {
			for(int j = 0; j < i; j++){
				if(array[j].compareTo(array[i]) > 1) {
					T temp = array[j];
					array[j] = array[i];
					array[i] = temp;
				}
			}
		}
		final long endTime = System.currentTimeMillis();
		System.out.println("Array Length: " + array.length +
				"\nSort Time: " + (endTime - startTime));
		return array;
	}
}