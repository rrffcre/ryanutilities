package Numbers.ExpressionTree;

/**
 * <h1>TreeNode</h1>
 * 
 * @author Ryan Plummer
 * @course CSCI 3320-001
 * 
 * @param <E>
 *            The type of object contained in this node.
 */
public class TreeNode<E extends Comparable <E>> {
	private E data;
	private TreeNode <E> leftNode;
	private TreeNode <E> rightNode;

	/**
	 * TreeNode(E d)
	 * <p>
	 * This constructor creates a new TreeNode that contains the given data.
	 * 
	 * @param d
	 *            The data that the new node contains.
	 */
	public TreeNode(E d) {
		this.data = d;
		this.leftNode = null;
		this.rightNode = null;
	}

	/**
	 * TreeNode(E d, TreeNode<E> l, TreeNode<E> r)
	 * <p>
	 * This constructor creates a new TreeNode that contains the given data and
	 * has the passed children nodes.
	 * 
	 * @param d
	 *            The data contained in the node.
	 * @param l
	 *            The node's left child.
	 * @param r
	 *            The node's right child.
	 */
	public TreeNode(E d, TreeNode <E> l, TreeNode <E> r) {
		this.data = d;
		this.leftNode = l;
		this.rightNode = r;
	}

	/**
	 * getData()
	 * <p>
	 * Gets the data stored in this node.
	 * 
	 * @return The stored data.
	 */
	public E getData() {
		return this.data;
	}

	/**
	 * setData(E d)
	 * <p>
	 * Stores the passed data in this node.
	 * 
	 * @param d
	 *            The data to be stored.
	 */
	public void setData(E d) {
		if (d != null) {
			this.data = d;
		}
	}

	/**
	 * getLeft()
	 * <p>
	 * Gets the left (lesser) child of this node.
	 * 
	 * @return The node to the left.
	 */
	public TreeNode <E> getLeft() {
		return this.leftNode;
	}

	/**
	 * setLeft(TreeNode<E> n)
	 * <p>
	 * Sets the passed node as the left child of this node.
	 * 
	 * @param n
	 *            The node being set to the left of this node.
	 */
	public void setLeft(TreeNode <E> n) {
		this.leftNode = n;
	}

	/**
	 * getRight()
	 * <p>
	 * Gets the right (greater) child of this node.
	 * 
	 * @return The node to the right.
	 */
	public TreeNode <E> getRight() {
		return this.rightNode;
	}

	/**
	 * setRight(TreeNode<E> n)
	 * <p>
	 * Sets the passed node as the right child of the node.
	 * 
	 * @param n
	 *            The node being set to the right of this node.
	 */
	public void setRight(TreeNode <E> n) {
		this.rightNode = n;
	}
}