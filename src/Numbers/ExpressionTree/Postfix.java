package Numbers.ExpressionTree;

import java.util.ArrayList;
import java.util.Stack;

/**
 * <h1>Postfix</h1>
 * 
 * @author Ryan Plummer
 * @course CSCI 3320-001
 */
public class Postfix {

	private static final String TIER_ONE_OPS = "[\\^]";
	private static final String TIER_TWO_OPS = "[\\*\\/\\%]";
	private static final String TIER_THREE_OPS = "[\\+\\-]";

	/**
	 * infixToPostfix(String infix)
	 * <p>
	 * Takes the passed infix expression and parses in into a postfix
	 * expression.
	 * 
	 * @param infix
	 *            Infix expression to be parsed.
	 * @return A postfix expression based on the passed string buffer.
	 * @throws IllegalArgumentException
	 */
	public static ArrayList <String> infixToPostfix(String infix) throws IllegalArgumentException {
		Stack <String> stack = new Stack <String>();
		String segment;
		int index = 0;
		stack.push("(");
		infix += (")");
		ArrayList <StringBuilder> infixList = new ArrayList <StringBuilder>();
		ArrayList <String> postfixList = new ArrayList <String>();
		char[] infixCharArray = infix.toCharArray();

		for (int i = 0; i < infix.length(); i++) {
			if (i == 0 || !Character.isDigit(infixCharArray[i]) || !Character.isDigit(infixCharArray[i - 1])) {
				infixList.add(new StringBuilder().append(infixCharArray[i]));
				index++;
			} else {
				infixList.get(index - 1).append(infixCharArray[i]);
			}
		}

		index = 0;
		while (!stack.isEmpty()) {
			segment = String.valueOf(infixList.get(index++));
			if (segment.matches("[\\d]+")) {
				postfixList.add(segment);
			} else if (segment.matches("[\\(]")) {
				stack.push(segment);
			} else if (segment.matches("[\\+\\-\\*\\/\\%\\^]")) {
				if (stack.peek().matches("[\\+\\-\\*\\/\\%\\^]")) {
					while (Postfix.compare(stack.peek(), segment) <= 0) {
						postfixList.add(stack.pop());
					}

				}
				stack.push(segment);
			} else if (segment.matches("[\\)]")) {
				while (!stack.peek().matches("[\\(]")) {
					postfixList.add(stack.pop());
				}
				stack.pop();
			} else {
				throw new IllegalArgumentException(segment);
			}
		}
		return postfixList;
	}

	/**
	 * compare(String str1, String str2)
	 * <p>
	 * Compares the passed mathematical operations based on the order of
	 * operations. If the first operation was a higher precedences than the
	 * second, 1 is returned. If the second operation was a higher precedences
	 * than the first, -1 is returned. If they have the same precedence, 0 is
	 * returned.
	 * 
	 * @param str1
	 *            The first operation.
	 * @param str2
	 *            The second operation.
	 * @return A numerical representation on the operations' relationship.
	 */
	private static int compare(String str1, String str2) {
		if ((str1.matches(TIER_THREE_OPS) && str2.matches(TIER_THREE_OPS))
				|| (str1.matches(TIER_TWO_OPS) && str2.matches(TIER_TWO_OPS))
				|| (str1.matches(TIER_ONE_OPS) && str2.matches(TIER_ONE_OPS))) {
			return 0;
		} else if ((str1.matches(TIER_TWO_OPS) && str2.matches(TIER_ONE_OPS)) || str1.matches(TIER_THREE_OPS)
				|| str1.matches("[\\(]")) {
			return 1;
		} else {
			return -1;
		}
	}
}