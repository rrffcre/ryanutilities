package Numbers.ExpressionTree;

import java.util.ArrayList;

/**
 * <h1>ExpressionTree</h1>
 * 
 * @author Ryan Plummer
 * @course CSCI 3320-001
 */
public class ExpressionTree {

	private TreeNode <String> root;
	private ArrayList <String> postfix;
	private int index;

	public ExpressionTree(String infix) {
		this.newExpression(infix);
	}

	public void newExpression(String infix) {
		infix = infix.replace(" ", "");
		this.postfix = Postfix.infixToPostfix(infix);
		this.index = this.postfix.size();
		this.root = this.createExpressionTree();
	}

	/**
	 * preOrder()
	 * <p>
	 * Prints all the nodes in the order which they are accessed.
	 */
	public void preOrder() {
		this.preOrderRecursive(this.root);
		System.out.println("");
	}

	/**
	 * preOrderRecursive(TreeNode<String> n)
	 * <p>
	 * Recursively prints the current node, the nodes to the left, then the
	 * nodes to the right.
	 * 
	 * @param n
	 *            The current node.
	 */
	private void preOrderRecursive(TreeNode <String> n) {
		if (n != null) {
			System.out.print(n.getData() + " ");
			this.preOrderRecursive(n.getLeft());
			this.preOrderRecursive(n.getRight());
		}
	}

	/**
	 * inOrder()
	 * <p>
	 * Prints all the nodes in ascending order.
	 */
	public void inOrder() {
		this.inOrderRecursive(this.root);
		System.out.println("");
	}

	/**
	 * inOrderRecursive(TreeNode<String> n)
	 * <p>
	 * Recursively prints the nodes tothe left, the current node, and the nodes
	 * to the right.
	 * 
	 * @param n
	 *            The current node.
	 */
	private void inOrderRecursive(TreeNode <String> n) {
		if (n != null) {
			this.inOrderRecursive(n.getLeft());
			System.out.print(n.getData() + " ");
			this.inOrderRecursive(n.getRight());
		}
	}

	/**
	 * postOrder()
	 * <p>
	 * Prints all the nodes in the order from the leaves to the root.
	 */
	public void postOrder() {
		this.postOrderRecursive(this.root);
		System.out.println("");
	}

	/**
	 * postOrderRecursive(TreeNode<String> n)
	 * <p>
	 * Recursively the nodes to the left, the nodes to the right, then prints
	 * the current node.
	 * 
	 * @param n
	 *            The current node.
	 */
	private void postOrderRecursive(TreeNode <String> n) {
		if (n != null) {
			this.postOrderRecursive(n.getLeft());
			this.postOrderRecursive(n.getRight());
			System.out.print(n.getData() + " ");
		}
	}

	/**
	 * evaluate()
	 * <p>
	 * This will return the numerical value of the expression tree.
	 * 
	 * @return The value of the expression.
	 */
	public int evaluate() {
		return this.evaluateRecursive(this.root);
	}

	/**
	 * evaluateRecursive(TreeNode<String> node)
	 * <p>
	 * This will recursively evaluate each node to calculate the total value.
	 * 
	 * @param node
	 *            The current node being evaluated.
	 * @return The value of the current node.
	 */
	private int evaluateRecursive(TreeNode <String> node) {
		String nodeValue = node.getData();
		if (nodeValue.matches("[\\d]+")) {
			return Integer.valueOf(nodeValue);
		} else {
			int right = this.evaluateRecursive(node.getRight());
			int left = this.evaluateRecursive(node.getLeft());
			switch (nodeValue.charAt(0)) {
			case '+':
				return right + left;
			case '-':
				return right - left;
			case '*':
				return right * left;
			case '/':
				return right / left;
			case '%':
				return right % left;
			case '^':
				return (int) Math.pow(right, left);
			default:
				return 0;
			}
		}
	}

	/**
	 * evaluate()
	 * <p>
	 * This will return the numerical value of the expression tree.
	 * 
	 * @return The value of the expression.
	 */
	public int evaluateWithSteps() {
		this.index = 1;
		return this.evaluateWithStepsRecursive(this.root);
	}

	/**
	 * evaluateRecursive(TreeNode<String> node)
	 * <p>
	 * This will recursively evaluate each node to calculate the total value.
	 * 
	 * @param node
	 *            The current node being evaluated.
	 * @return The value of the current node.
	 */
	private int evaluateWithStepsRecursive(TreeNode <String> node) {
		String nodeValue = node.getData();
		if (nodeValue.matches("[\\d]+")) {
			return Integer.valueOf(nodeValue);
		} else {
			int right = this.evaluateWithStepsRecursive(node.getRight());
			int left = this.evaluateWithStepsRecursive(node.getLeft());
			int result;
			switch (nodeValue.charAt(0)) {
			case '+':
				result = right + left;
				break;
			case '-':
				result = right - left;
				break;
			case '*':
				result = right * left;
				break;
			case '/':
				result = right / left;
				break;
			case '%':
				result = right % left;
				break;
			case '^':
				result = (int) Math.pow(right, left);
				break;
			default:
				return 0;
			}
			System.out.println("Step " + (this.index++) + ": " + right + nodeValue + left + " = " + result);
			return result;
		}
	}

	/**
	 * createExpressionTree()
	 * <p>
	 * Creates the expression tree recursively from a postfix expression.
	 * 
	 * @return A node containing the data from the postfix expression.
	 */
	private TreeNode <String> createExpressionTree() {
		String item = this.postfix.get(--this.index);
		if (item.matches("[\\d]+")) {
			return new TreeNode <String>(item, null, null);
		} else {
			TreeNode <String> left = createExpressionTree();
			TreeNode <String> right = createExpressionTree();
			return new TreeNode <String>(item, left, right);
		}
	}
}
