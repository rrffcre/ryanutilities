package Numbers;

import java.awt.Dimension;
import java.util.logging.Logger;

public class Matrix {

	private static Logger solve = Logger.getLogger("Solve");
	private static Logger problem = Logger.getLogger("problem");

	private double[][] matrix;
	private double determinant = Double.NaN;

	private int height;
	private int width;

	/**
	 * 
	 * @param r
	 * @param c
	 */
	public Matrix(int r, int c) {
		this.matrix = new double[r][c];
		this.height = r;
		this.width = c;
	}

	/**
	 * 
	 * @param d
	 */
	public Matrix(Dimension d) {
		this.matrix = new double[d.height][d.width];
		this.height = d.height;
		this.width = d.width;
	}

	/**
	 * 
	 * @param v
	 */
	public Matrix(double[][] v) {
		this.matrix = v;
		this.height = v.length;
		this.width = v[0].length;
	}

	/**
	 * 
	 * @param size
	 * @return
	 */
	public static Matrix getUnitMatrix(int size) {
		Matrix m = new Matrix(size, size);
		for (int ir = 0; ir < size; ir++) {
			for (int ic = 0; ic < size; ic++) {
				if (ir == ic) {
					m.set(ir, ic, 1.0);
				}
			}
		}
		return m;
	}

	/**
	 * 
	 * @param log
	 * @return
	 */
	public static Matrix getInverse(Matrix matrix, boolean log) {
		if (matrix.height != matrix.width) {
			Matrix.problem.warning("Matrix must be square to have an inverse");
			return null;
		} else if (Matrix.getDeterminant(matrix, false) == 0) {
			Matrix.problem.warning("Matrix must have a non-zero determinant");
			return null;
		}
		Matrix copy = Matrix.getCopy(matrix);
		Matrix unit = Matrix.getUnitMatrix(copy.height);
		int stepNum = 1;
		for (int ic = 0; ic < copy.width; ic++) {
			for (int ir = 0; ir < copy.height; ir++) {
				String step = "";
				if (ir == ic && copy.get(ir, ic) != 1.0) {
					double inverse = 1.0 / copy.get(ir, ic);
					step += "Step " + stepNum + "\tR" + ir + " = R" + ir + " * " + inverse;
					copy.rowMultiplication(ir, inverse);
					unit.rowMultiplication(ir, inverse);
				} else if (ir != ic && copy.get(ir, ic) != 0.0) {
					double ma = -copy.get(ir, ic) / copy.get(ic, ic);
					step += "Step " + stepNum + "\tR" + ir + " = R" + ir + " + R" + ic + "*" + ma;
					copy.rowAddition(ir, 1, ic, ma);
					unit.rowAddition(ir, 1, ic, ma);
				}

				if (log && !step.isEmpty()) {
					step += "\nCurrent Matrix Status:\n" + copy + "Identity Matrix Status:\n" + unit;
					Matrix.solve.info(step);
					stepNum++;
				}
			}
		}
		unit.determinant = 1.0 / matrix.determinant;
		return unit;
	}

	/**
	 * 
	 */
	public static Matrix transpose(Matrix matrix) {
		Matrix trans = new Matrix(matrix.width, matrix.height);
		for (int ir = 1; ir < matrix.height; ir++) {
			for (int ic = 0; ic < matrix.width; ic++) {
				trans.set(ic, ir, matrix.get(ir, ic));
			}
		}
		return trans;
	}

	public static Matrix multiply(Matrix ma, Matrix mb) {
		if (ma.width != mb.height) {
			Matrix.problem.warning("Matrix dimensions are not compatible");
			return null;
		}
		Matrix result = new Matrix(ma.height, mb.width);

		for (int ir = 0; ir < ma.height; ir++) {
			for (int ic = 0; ic < mb.width; ic++) {
				double total = 0.0;
				for (int i = 0; i < ma.width; i++) {
					total += ma.get(ir, i) * mb.get(i, ic);
				}
				result.set(ir, ic, total);
			}
		}

		return result;
	}

	/**
	 * 
	 * @param log
	 * @return
	 */
	public static double getDeterminant(Matrix matrix, boolean log) {
		String step = "Determinant for\n" + matrix;
		double det = 0.0;

		if (!Double.isNaN(matrix.determinant)) {
			step += "Found\n";
			det = matrix.determinant;
		} else if (matrix.height == 2) {
			det = (matrix.get(0, 0) * matrix.get(1, 1)) - (matrix.get(0, 1) * matrix.get(1, 0));
		} else {
			double sign = 1;
			for (int c = 0; c < matrix.width; c++) {
				Matrix sub = new Matrix(matrix.height - 1, matrix.width - 1);
				int sr = 0;
				for (int ir = 1; ir < matrix.height; ir++) {
					int sc = 0;
					for (int ic = 0; ic < matrix.width; ic++) {
						if (ic != c) {
							sub.set(sr, sc, matrix.get(ir, ic));
							sc++;
						}
					}
					sr++;
				}
				det += (matrix.get(0, c) * Matrix.getDeterminant(sub, log) * sign);
				sign *= -1;
			}
		}

		// Log step
		if (log) {
			step += " = " + det;
			Matrix.solve.info(step);
		}

		// Save value for future use
		if (Double.isNaN(matrix.determinant)) {
			matrix.determinant = det;
		}
		return det;
	}

	public static Matrix getCopy(Matrix old) {
		Matrix copy = new Matrix(old.height, old.width);
		for (int ir = 0; ir < old.height; ir++) {
			for (int ic = 0; ic < old.width; ic++) {
				copy.set(ir, ic, old.get(ir, ic));
			}
		}
		return copy;
	}

	public double get(int row, int col) {
		return this.matrix[row][col];
	}

	public boolean set(int row, int col, double val) {
		if (row < 0 || row >= this.height) {
			Matrix.problem.warning("Row index is out of bounds: " + row);
			return false;
		} else if (col < 0 || col >= this.width) {
			Matrix.problem.warning("Column index is out of bounds: " + col);
			return false;
		} else if (Double.isNaN(val) || Double.isInfinite(val)) {
			Matrix.problem.warning("Value is not valid");
			return false;
		} else {
			this.matrix[row][col] = val;
			this.determinant = Double.NaN;
			return true;
		}
	}

	@Override
	public String toString() {
		String string = "";
		for (int ir = 0; ir < this.height; ir++) {
			string += "|";
			for (int ic = 0; ic < this.width; ic++) {
				string += " " + String.format("%4.4f", this.get(ir, ic)) + " ";
			}
			string += "|\n";
		}
		return string;
	}

	private boolean rowAddition(int re, double me, int ra, double ma) {
		if (re < 0 || re >= this.height || ra < 0 || ra >= this.height) {
			Matrix.problem.warning("Row index is out of bounds");
			return false;
		}
		for (int i = 0; i < this.width; i++) {
			this.set(re, i, (this.get(re, i) * me) + (this.get(ra, i) * ma));
		}
		return true;
	}

	private boolean rowMultiplication(int r, double m) {
		if (r < 0 || r >= this.height) {
			Matrix.problem.warning("Row index is out of bounds");
			return false;
		}
		for (int i = 0; i < this.width; i++) {
			if (this.get(r, i) != 0) {
				this.set(r, i, this.get(r, i) * m);
			}
		}
		return true;
	}

}
