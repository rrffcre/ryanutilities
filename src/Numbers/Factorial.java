package Numbers;

/**
 * <h1>Factorial</h1>
 * 
 * @author Ryan Plummer
 *
 */
public class Factorial {

	private Integer[] factList;

	public int fact(int num) {
		if (this.factList == null) {
			this.factList = new Integer[num];
		} else if (num > this.factList.length) {
			Integer[] old = this.factList;
			this.factList = new Integer[num];
			for (int i = 0; i < old.length; i++) {
				this.factList[i] = old[i];
			}
		}
		return this.factRecursive (num).intValue ();
	}

	private Integer factRecursive(Integer n) {
		if (this.factList[n] != null) {
			return this.factList[n];
		} else if (n == 1) {
			return 1;
		}
		this.factList[n] = n * this.factRecursive (n - 1);
		return this.factList[n];
	}

}