package DataStructures;

public class LinkedRing<T> {

	/**
	 * A container class for data of type S.
	 * 
	 * @author Ryan
	 *
	 * @param <S>
	 */
	private class Node<S> {
		private S data;
		private Node <S> next;
		private Node <S> prev;

		Node() {
			this.data = null;
			this.next = this;
			this.prev = this;
		}

		Node(S data) {
			this.data = data;
			this.next = this;
			this.prev = this;
		}

		public S getData() {
			return this.data;
		}

		public Node <S> getNext() {
			return this.next;
		}

		public Node <S> getPrev() {
			return this.prev;
		}

		public void setData(S data) {
			this.data = data;
		}

		public void setNext(Node <S> next) {
			this.next = next;
		}

		public void setPrev(Node <S> prev) {
			this.prev = prev;
		}
	}

	private int size;
	private Node <T> current;

	/*
	 * Construct an empty Linked Ring.
	 */
	LinkedRing() {
		this.size = 0;
		this.current = new Node <>();
	}

	/**
	 * Construct a new LinkedList with a single node containing the given data.
	 * 
	 * @param data
	 *          Data for the first node.
	 */
	LinkedRing(T data) {
		this.size = 1;
		this.current = new Node <>(data);
	}

	/**
	 * Creates a new node with the given data and insert it after the current
	 * node.
	 * 
	 * @param data
	 *          Data for the first node.
	 */
	public void insertAfterCurrent(T data) {
		if (this.current.getData() == null) {
			this.current.setData(data);
		} else {
			Node <T> dataNode = new Node <>(data);
			Node <T> first = this.current.getNext();

			first.setPrev(dataNode);
			this.current.setNext(dataNode);

			dataNode.setNext(first);
			dataNode.setPrev(this.current);

			this.current = dataNode;
		}
		this.size++;
	}

	/**
	 * Removes the current node and closes the ring around the gap.
	 * 
	 * @return The data contained in the current node.
	 */
	public T removeCurrent() {
		T data = this.current.getData();
		Node <T> next = this.current.getNext();

		this.current = this.current.getPrev();
		next.setPrev(this.current);
		this.current.setNext(next);

		this.size--;
		return data;
	}

	/**
	 * Returns the current number of nodes in the ring.
	 * 
	 * @return The number of nodes.
	 */
	public int getSize() {
		return this.size;
	}

	/**
	 * Change the current node to the next node on the right.
	 * 
	 * @return The value of the new current node.
	 */
	public T rotateRight() {
		this.current = this.current.getNext();
		return this.current.getData();
	}

	/**
	 * Change the current node to the next node on the left.
	 * 
	 * @return The value of the new current node.
	 */
	public T rotateLeft() {
		this.current = this.current.getPrev();
		return this.current.getData();
	}

	/**
	 * Sets the current node to a node that matches the given data.
	 * 
	 * @param data
	 *          Data of the node to rotate to.
	 * @return If a node with the given data exists.
	 */
	public boolean rotateTo(T data) {
		if (this.contains(data)) {
			while (!this.rotateRight().equals(data))
				;
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Determine if a node with the given data exists.
	 * 
	 * @param data
	 *          Data of the node to search for.
	 * @return If a node with the given data exists.
	 */
	public boolean contains(T data) {
		Node <T> original = this.current;
		boolean found = false;

		do {
			if (this.rotateRight().equals(data)) {
				found = true;
				break;
			}
		} while (this.current.equals(original));

		this.current = original;
		return found;
	}
}